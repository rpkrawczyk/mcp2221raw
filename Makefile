#! /usr/bin/male -f
#make CXXFLAGS="-std=c++14 -O2 -Wall -Wextra -DDEBUG"

CXXFLAGS=-std=c++14 -Wall -Wextra -O3 -DNDEBUG

##################################################################
COMMONOBJ=transforminput.o
prefix = $(DESTDIR)/usr/local
exec_prefix = $(prefix)
bindir = $(exec_prefix)/bin
##################################################################

all: mcp2221raw

mcp2221raw: mcp2221raw.o device.o $(COMMONOBJ)
	$(CXX) $(CXXFLAGS) -o $@ $+

.PHONY: check
check:	runner🖬
	./runner🖬

runner🖬.cc: transforminput.hh
	cxxtestgen --error-printer -o runner🖬.cc transforminput.hh

runner🖬: runner🖬.o $(COMMONOBJ)
	$(CXX) -std=c++14 -o $@ $+

.PHONY: doc
doc:
	doxygen

install:
	install -s -t $(bindir) mcp2221raw

uninstall:
	rm $(bindir)/mcp2221raw

.PHONY: install uninstall

.PHONY:	all clean
clean:
	rm -f *.o mcp2221raw
	rm -f *🖬*
	rm -rf doc

