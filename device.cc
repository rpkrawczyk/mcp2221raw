#include <linux/types.h>
#include <linux/input.h>
#include <linux/hidraw.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "device.hh"
#include <stdexcept>
#include <sstream>
#include <iostream>

MCP2221Device::~MCP2221Device() {
  close(get_fd());
}


MCP2221Device::MCP2221Device(const char *devname) {
  int res;
  char buf[256];
  
  device_fd = open(devname, O_RDWR|O_NONBLOCK);
  if (device_fd < 0) {
    std::ostringstream out;
    out << "unable to open device: " << strerror(errno);
    throw std::runtime_error(out.str());
  } else {
    memset(buf, 0x0, sizeof(buf));
  }
  try {
    /* Get Raw Name */
    res = ioctl(device_fd, HIDIOCGRAWNAME(256), buf);
    if (res < 0) {
      std::ostringstream out;
      out << "HIDIOCGRAWNAME: " << strerror(errno);
      throw std::runtime_error(out.str());
    } else {
      //printf("Raw Name: %s\n", buf);
      std::string raw_name(buf);
      if(raw_name.find("MCP2221") == std::string::npos) {
	throw std::runtime_error("unknown device " + raw_name);
      }
    }
  }
  catch(const std::exception &) {
    close(device_fd);
    throw;
  }
#ifdef DEBUG
  std::cerr << "Debug: file descriptor is " << device_fd << std::endl;
#endif
}


ByteArray MCP2221Device::read() {
  int res;
  std::ostringstream out;
  unsigned char buf[64];
//   const unsigned int step = 0x10 - 1;
  
  for(int ji = 0; ji < 16; ++ji) {
    /* Get a report from the device */
    res = ::read(get_fd(), buf, 64);
    if (res < 0) {
      if(errno != EAGAIN) {
	out << "reading faile due to " << strerror(errno);
	throw std::runtime_error(out.str());
      }
    } else {
      //printf("read() read %d bytes:", res);
      return std::vector<unsigned char>(buf, buf + res);
    }
    sleep(1);
  }
  throw std::runtime_error("read timed out");
}


int MCP2221Device::write(const std::vector<uint8_t> &out) {
  std::vector<uint8_t> buf(out);
  int res;

  if(buf.size() >= 64) {
    std::cerr << "Warning! Truncating write to 64 bytes.\n";
  }
  buf.resize(64);
  res = ::write(get_fd(), &buf[0], 64);
  if (res < 0) {
    throw std::runtime_error(strerror(errno));
  } else {
    //Everything ok.
  }
  return res;
}

