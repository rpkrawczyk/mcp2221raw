# mcp2221raw

This is a tool to talk the MCP2221 hidraw device. With this tool it is
possible to configure the MCP2221 and to use the GPIOs.

The MCP2221 is a low-cost component to connect a serial bus to USB. It
even has four GPIO pins which can be configured using this tool.

# Compilation

This simple tool does need the following external libraries and tools:

 * boost C++ library
 * cxxtest (for unit tests)
 * Doxygen (for the documentation, optional)

They can be installed on a Debian-like system using:

	sudo apt-get install libboost-dev cxxtest doxygen

After the prerequisites have been installed a simple `make` should do:

	make
	make check
	make install

If you like a nicely formatted documentation then `make doc` is your
friend. With this command the HTML documentation is generated and can
be found in the `doc/html` directory.

# Schematics

The MCP2221 is very simple to use. In the simplest form just connect
the chip to the usb bus. That's it! It will work. For added stability
use capacitors from the +5V and the VUSB to enhance reliability. A
simple diagram with a nice received serial data indicator.

![Simple schematics](schematics.png)

# Usage

Make sure that you have the appropriate rights to access the
device. Either perform the commands as root (dangerous!) or create a
group and add the user to this group and set the owner of the device
to this group. The program can be run using:

	./mcp2221raw

if your mcp2221 defaults to `/dev/hidraw0`. A typical session looks like this:

	$ sudo ./mcp2221raw /dev/hidraw2
	MCP object created at 0x7ffecd3cbab0.
	$51
	< 51
	> 51 00 ee ef 00 00 ee ef ee ef 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  Q...............................................................
	gpioallout
	< 60 ea 00 00 00 00 00 ff 00 00 00 00
	> 60 00 ee ef 00 00 ee ef ee ef 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00  `...............................................................

As you can see my mcp2221 is known to the system as `/dev/hidraw2`.

Lines starting with a '$' or a space will be interpreted as literal
hex bytes and send to the device. **Warning!** There are no safeguards
whatsoever. Everything will be sent *literally*. If you bork your
device then this is your fault. *You have been warned!*

The lines starting with a '<' (lesser sign) are the bytes which are
sent to the device. The lines starting with a '>' (greater sign) are
the bytes received from the device. For a list of commands have a look
at transforminput.hh or transforminput.cc.

# Links

 * https://bitbucket.org/rpkrawczyk/mcp2221raw/
 * https://www.microchip.com/wwwproducts/en/MCP2221
