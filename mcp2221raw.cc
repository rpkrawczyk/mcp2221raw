#include "transforminput.hh"


/*!\brief Read from stdin and write to device
 *
 * This function will read from stdin and transform the input line
 * using a TransformInput object. Then command data-bytes are written
 * to the device and the return 64 byte block is displayed.
 * 
 * \param mcp reference to MCP2221Device object
 */
void loop(MCP2221Device &mcp) {
  std::string line;
  TransformInput transformer;

  while(std::getline(std::cin, line)) {
    try {
      std::vector<uint8_t> data(transformer(line));
      if(!data.empty()) {
	std::cout << '<' << data << std::endl;
	mcp.write(data);
	data = mcp.read();
	std::cout << '>' << data << "  " << datastringdump(data) << std::endl;
      }
      //Clear the ByteArray, just to be sure.
      data.clear();
    }
    catch(const std::invalid_argument &excp) {
      std::cerr << "Error: " << excp.what() << std::endl;
    }
  }
}


/*! \brief Main function called from CLI.
 *
 * The main function is called from the command-line interface. It
 * will open a mcp2221 device using the MCP2221Device class and then
 * read commands from the standard input. These commands are processed
 * in the loop() function.
 * 
 * \param argc number of arguments
 * \param argv pointers to arguments
 */
int main(int argc, char **argv) {
  const char *device = "/dev/hidraw0";

  if(argc > 1) {
    device = argv[1];
  }
  try {
    MCP2221Device mcp2221(device);
    std::cerr << "MCP object created at " << &mcp2221 << ".\n";
    loop(mcp2221);
  }
  catch(const std::exception &excp) {
    std::cerr << "Error: " << excp.what() << std::endl;
    return -1;
  }
  return 0;
}

//! \file
