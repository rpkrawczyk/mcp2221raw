#ifndef __TRANSFORMINPUT_HH_2018__
#define __TRANSFORMINPUT_HH_2018__
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <numeric>
#include <boost/format.hpp>
#include <cxxtest/TestSuite.h>
#include "device.hh"

/*! \brief Output ByteArray
 *
 * Convenience function to output a ByteArray. This will print a
 * simple hex representation.
 *
 * @param out output stream
 * @param data reference to a ByteArray
 * @return output stream
 */
std::ostream &operator<<(std::ostream &out, const ByteArray &data);

/*! \brief Dump ByteArray in a string representation
 *
 * Get a string representation of a ByteArray.
 *
 * @param data reference to a ByteArray
 * @return string with non-printable characters replaced by a '.'
 */
std::string datastringdump(const ByteArray &data);

/*! \brief Transform the text input into bytes for device commands
 *
 * This is a very simple parser which will generate the byte arrays to
 * be sent to the device.
 */
class TransformInput {
protected:
  /*!\brief Interpret a command string
   *
   * This function will throw an exception if the input can not be
   * parsed.
   *
   * \param inp input string stream
   * \return interpreted input convert to the corresponding command bytes
   */
  ByteArray command(std::istringstream &inp);
  /*!\brief Interpret raw hex string
   *
   * This function will interpret a raw hex string into its ByteArray
   * form. Characters are read until the end of the stream thus the
   * resulting array may be longer than 64 bytes.
   *
   * Attention! The hex string has to start with a space, a tab, or a
   * dollar sign!
   *
   * \param inp input string stream
   * \return interpreted input convert to the corresponding command bytes
   * \throw std::logic_error if string is malformed
   */
  ByteArray raw_hex(std::istringstream &inp);
  
public:
  /*!\brief Make object callable
   *
   * This will interpret the given string and return the ByteArray
   * with the corresponding command bytes. The commands are described
   * in the \ref commandreferencepage.
   *
   * \param line string to interpret
   * \return command bytes
   * \throw Exception is thrown on any error.
   */
  ByteArray operator()(const std::string &line);
};


/* ------------------------------------------------------------------ */
class MyTestSuite1 : public CxxTest::TestSuite {
  TransformInput tinp;
public:
  void setUp(void) {
  }
  void tearDown(void) {
  }
  void testRawHex(void) {
    ByteArray myvec{0};
    ByteArray deadbea1{0xde, 0xad, 0xbe, 0xa1};

    TS_ASSERT_EQUALS(tinp("$00"), myvec);
    myvec.push_back(0xff);
    TS_ASSERT_EQUALS(tinp("$00 ff"), myvec);
    TS_ASSERT_EQUALS(tinp("$de ad be a1"), deadbea1);
    TS_ASSERT_EQUALS(tinp(" de ad be a1"), deadbea1);
    TS_ASSERT_EQUALS(tinp("\tde ad be a1"), deadbea1);
  }
  void testCommands(void) {
    TS_ASSERT_EQUALS(tinp("readchip"), ByteArray({0xb0, 0x00}));
    TS_ASSERT_EQUALS(tinp("readgp"), ByteArray({0xb0, 0x01}));
    TS_ASSERT_EQUALS(tinp("readmanufacturer"), ByteArray({0xb0, 0x02}));
    TS_ASSERT_EQUALS(tinp("readproduct"), ByteArray({0xb0, 0x03}));
    TS_ASSERT_EQUALS(tinp("readserial"), ByteArray({0xb0, 0x04}));
  }
  void testThrows(void) {
    //Errors while compiling? TS_ASSERT_THROWS(tinp("non-existent"), std::invalid_argument);
    TS_ASSERT_THROWS_ANYTHING(tinp("non-existent"));
  }
};

class MyTestSuite2 : public CxxTest::TestSuite {
  ByteArray ascending;
public:
  void setUp(void) {
    ascending.resize(256);
    std::iota(ascending.begin(), ascending.end(), 0);
  }
  void testDump(void) {
    ByteArray myvec{0};

    TS_ASSERT_EQUALS(datastringdump(myvec), ".");
    TS_ASSERT_EQUALS(datastringdump({0, 0xff}), "..");
  }
  void testDigits(void) {
    TS_ASSERT_EQUALS(datastringdump({0x30, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x39}), "0123456789");
  }
  void testAscending(void) {
    std::string ascending_str(datastringdump(ascending));
    /*
     * 5 * 16 characters, '.' and DEL are mapped to '.'.
     */
    TS_ASSERT_EQUALS(std::count(ascending_str.begin(), ascending_str.end(), '.'), 162);
    TS_ASSERT_EQUALS(ascending_str.find('\x7f'), std::string::npos);
    TS_ASSERT_EQUALS(ascending_str.find("\r\n"), std::string::npos);
    TS_ASSERT_EQUALS(ascending_str.find(' '), 32);
    TS_ASSERT_EQUALS(ascending_str.find('~'), 0x7E);
    TS_ASSERT_EQUALS(ascending_str.find('`'), 0x60);
  }
  void testOutputOp(void) {
    std::ostringstream out;
    out << ascending;
    std::string str(out.str());
    TS_ASSERT_EQUALS(str.size(), 256*3);
    TS_ASSERT_EQUALS(std::count(str.begin(), str.end(), ' '), 256);
  }
};

/*! \file
 *
 */
#endif
