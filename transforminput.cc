#include "transforminput.hh"

std::ostream &operator<<(std::ostream &out, const ByteArray &data) {
  for(int i : data) {
    out << boost::format(" %02x") % i;
  }
  return out;
}

std::string datastringdump(const ByteArray &data) {
  std::string out(data.size(), '.');

  std::transform(data.begin(), data.end(), out.begin(), [](uint8_t x) { if(x < ' ' || x >= 127) {
	return '.';
      } else {
	return static_cast<char>(x);
      }});
  return out;
}


ByteArray TransformInput::command(std::istringstream &inp) {
  std::string command;

  inp >> command;
  if(command == "readchip") {
    return ByteArray {0xb0, 0x00};
  } else if(command == "readgp") {
    return ByteArray {0xb0, 0x01};
  } else if(command == "readmanufacturer") {
    return ByteArray {0xb0, 0x02};
  } else if(command == "readproduct") {
    return ByteArray {0xb0, 0x03};
  } else if(command == "readserial") {
    return ByteArray {0xb0, 0x04};
  } else if(command == "readchipfactoryserial") {
    return ByteArray {0xb0, 0x05};
  } else if(command == "gpioallout") {
    uint8_t vals[4] = {0, 0, 0, 0};
    unsigned int ival;
    if(inp >> ival) {
      vals[0] = ival == 0 ? 0 : (1<<4);
      if(inp >> ival) {
	vals[1] = ival == 0 ? 0 : (1<<4);
	if(inp >> ival) {
	  vals[2] = ival == 0 ? 0 : (1<<4);
	  if(inp >> ival) {
	    vals[3] = ival == 0 ? 0 : (1<<4);
	  }
	}
      }
    }
    /*
     * At position seven the documentation says to write a $01 to
     * alter the GPIO settings. This never worked for me therefore a
     * $FF is written. With this value it seems to work.
     */
    return ByteArray {0x60, 0xea, 0, 0, 0, 0, 0, 0xFF, vals[0], vals[1], vals[2], vals[3]};
  }
  throw std::invalid_argument("unknown command: " + command);
}


ByteArray TransformInput::raw_hex(std::istringstream &inp) {
  int val;
  char ch;
  ByteArray data;

  inp.get(ch); //Skip one character.
  if(ch != ' ' && ch != '\t' && ch != '$') {
    throw std::logic_error("hex with wrong initial char");
  }
  //Raw hex input.
  while(inp >> std::hex >> val) {
    data.push_back(val);
  }
  return data;
}


ByteArray TransformInput::operator()(const std::string &line) {
  ByteArray data;
  std::istringstream tokline(line);

  if(!line.empty()) {
    switch(line[0]) {
    case '$':
    case ' ':
    case '\t':
      data = raw_hex(tokline);
      break;
    case 'a':
    case 'b':
    case 'c':
    case 'd':
    case 'e':
    case 'f':
    case 'g':
    case 'h':
    case 'i':
    case 'j':
    case 'k':
    case 'l':
    case 'm':
    case 'n':
    case 'o':
    case 'p':
    case 'q':
    case 'r':
    case 's':
    case 't':
    case 'u':
    case 'v':
    case 'w':
    case 'x':
    case 'y':
    case 'z':
      data = command(tokline);
      break;
    case '#':
      //Comment.
      break;
    default:
      throw std::runtime_error(str(boost::format("unknown character '%c'") % line[0]));
    }
  }
  return data;
}

/*! \file
 
  \page commandreferencepage Command reference page
  
  \section Command Usage
  
  The TransformInput::operator()(const std::string &line) method will
  transfrom the input to the following rules. Any line beginning with
  a space, tab, or dollar sign is interpreted as literal hex values.
 
  \section Commands

  \subsection readchip

  Read Chip Settings – it will read the MCP2221 flash settings, see
  table 3.3 in the MCP2221 documentation.

  \subsection readgp
 
  Read GP Settings – it will read the MCP2221 flash GP settings.

  \subsection readmanufacturer

  Read USB Manufacturer Descriptor String – reads the USB Manufacturer
  String Descriptor used during the USB enumeration.

  \subsection readproduct

  Read USB Product Descriptor String – reads the USB Product String
  Descriptor used during the USB enumeration.

  \subsection readserial

  Read USB Serial Number Descriptor String – reads the USB Serial
  Number String Descriptor that is used during USB enumeration. This
  serial number can be changed by the user through a specific USB HID
  command.

  \subsection readchipfactoryserial

  Read Chip Factory Serial Number – reads the factory-set serial
  number. This serial number cannot be changed.

  \subsection gpioallout

  This command will set all the GPIO pins to GPIO mode and they
  default to output. Up to four optional parameters (either 0 or 1)
  can be added and they will describe the output level (low or
  high). The default is low for all four GPIO pins.
 */
