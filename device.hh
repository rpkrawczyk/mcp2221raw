#ifndef __DEVICE_HH_20180305
#define __DEVICE_HH_20180305
#include <vector>
#include <stdint.h>

/*!\brief Simple class representing bytes to be sent to the device.
 *
 * This will represent an array of bytes which can be sent to the
 * device.
 */
typedef std::vector<uint8_t> ByteArray;

/*!\brief Class to communicate with MCP2221.
 *
 * This class encapsulates a very simple interface to the MCP2221
 * device using the HIDRAW USB communication. It will actually just
 * open a device using the corresponding device file. The file is kept
 * open until the object is destroyed.
 */
class MCP2221Device {
  int device_fd; //!< file descriptor for the MCP2221 device
protected:
  /*! \brief get file descriptor
   *
   * Get the file descriptor for the device
   *
   * \return file descriptor
   */
  int get_fd() const {
    return device_fd;
  }
public:
  /*!\brief Constructor
   *
   *\param devname device file-name
   */
  MCP2221Device(const char *devname);
  /*!\brief Desctructor
   *
   * This will close the device.
   */
  ~MCP2221Device();
  
  /*! \brief Read a block from the device
   *
   * Read a 64 byte block from the device and return as a ByteArray
   * for further processing.
   *
   * @return bytes read from device
   */
  ByteArray read();
  /*! \brief write to device
   *
   * Write bytes to device. Only the first 64 bytes are considered.
   * \param out vector of byte to write
   * \return result from kernal call (see write)
   */
  int write(const std::vector<uint8_t> &out);
};

/*!\file device.hh
 * Device communication header.
 *
 * Also defines important data types.
 */
#endif
